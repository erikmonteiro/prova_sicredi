package paginas;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BasePage {
	
	WebDriver driver;
	WebDriverWait espera;
	String url;
	
	public BasePage(String url) {
		
		this.url = url;
		
		driver = new ChromeDriver();
		espera = new WebDriverWait(driver, 5);
		
	}
	
	public WebDriver getDriver() {
		return driver;
	}

	public WebDriverWait getEspera() {
		return espera;
	}

	public String getUrl() {
		return url;
	}

	public void abrirBrowser() {

		driver.manage().deleteAllCookies();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
		
		driver.get(url);
		
	}
	
	public void fecharNavegador() {
		
		driver.close();
		
	}
	


}
