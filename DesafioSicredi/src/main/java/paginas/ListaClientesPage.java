package paginas;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class ListaClientesPage {
	
	WebDriver driver;
	WebDriverWait espera;
	
	By seletorTema = (By.id("switch-version-select"));
	By buscaNome = (By.name("customerName")); 
	By selecionaTodos = (By.className("select-all-none"));
	By excluir = (By.linkText("Delete"));
	By popupConfirmaExclusao = (By.className("alert-delete-multiple-one"));
	By botaoConfirmaExclusao = (By.className("delete-multiple-confirmation-button"));
	By popupUsuarioExcluido = (By.xpath("//span[3]/p"));
	By adicionarCliente = (By.linkText("Add Record"));
	
	String mensagemConfirmaExclusao =  "Are you sure that you want to delete this 1 item?";
	String mensagemUsuarioExcluido = "Your data has been successfully deleted from the database.";

	public ListaClientesPage(BasePage navegador) {
		
		driver = navegador.driver;
		espera = navegador.espera;
	}
	
	public void mudarTema(String tema){
		
		String temaXpath = "//option[contains(text(),'" + tema + "')]"; 
			
		driver.findElement(seletorTema).click();
		driver.findElement(By.xpath(temaXpath)).click();
		
	}
	
	public void buscarCliente(String nome) {
		
		driver.findElement(buscaNome).sendKeys(nome);
		espera.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//td[contains(.,'"+ nome +"')]")));
		Assert.assertTrue(driver.findElement(By.xpath("//td[contains(.,'"+ nome +"')]")).isDisplayed());;		
		
	}
	
	public void deletarCliente(String nome) {
		
		buscarCliente(nome);
		driver.findElement(selecionaTodos).click();
		driver.findElement(excluir).click();
		espera.until(ExpectedConditions.visibilityOfElementLocated(popupConfirmaExclusao));
		Assert.assertEquals(driver.findElement(popupConfirmaExclusao).getText(), mensagemConfirmaExclusao);
		driver.findElement(botaoConfirmaExclusao).click();
		espera.until(ExpectedConditions.visibilityOfElementLocated(popupUsuarioExcluido));
		Assert.assertEquals(driver.findElement(popupUsuarioExcluido).getText(), mensagemUsuarioExcluido);
		
	}
	
	public void criarCliente() {
		driver.findElement(adicionarCliente).click();
	}

}
