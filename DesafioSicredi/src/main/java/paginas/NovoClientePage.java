package paginas;

import java.util.ArrayList;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;


public class NovoClientePage {
	
	WebDriver driver;
	WebDriverWait espera;
	
	By nome = (By.id("field-customerName"));
	By ultimoNome = (By.id("field-contactLastName"));
	By nomeContato = (By.id("field-contactFirstName"));
	By fone = (By.id("field-phone"));
	By endereco = (By.id("field-addressLine1"));
	By complemento = (By.id("field-addressLine2"));
	By cidade = (By.id("field-city"));
	By uf = (By.id("field-state"));
	By cep = (By.id("field-postalCode"));
	By pais = (By.id("field-country"));
	By empregador = (By.id("field-salesRepEmployeeNumber"));
	By limiteCredito = (By.id("field-creditLimit"));
	By salvar =  (By.id("form-button-save"));
	By inclusaoSucesso = (By.id("report-success"));
	By voltarParaLista = (By.linkText("Go back to list"));
	By salvarEVoltarParaLista = (By.id("save-and-go-back-button"));
	
	String mensagemInclusaoSucesso = "Your data has been successfully stored into the database. Edit Record or Go back to list";
	
	public NovoClientePage(BasePage navegador) {
		driver = navegador.driver;
		espera = navegador.espera; 

			
	}
	

	
	public void adicionarCliente(ArrayList<String> cliente) {
		
		driver.findElement(nome).sendKeys(cliente.get(0));
		driver.findElement(ultimoNome).sendKeys(cliente.get(1));
		driver.findElement(nomeContato).sendKeys(cliente.get(2));
		driver.findElement(fone).sendKeys(cliente.get(3));
		driver.findElement(endereco).sendKeys(cliente.get(4));
		driver.findElement(complemento).sendKeys(cliente.get(5));
		driver.findElement(cidade).sendKeys(cliente.get(6));
		driver.findElement(uf).sendKeys(cliente.get(7));
		driver.findElement(cep).sendKeys(cliente.get(8));
		driver.findElement(pais).sendKeys(cliente.get(9));
		driver.findElement(empregador).sendKeys(cliente.get(10));
		driver.findElement(limiteCredito).sendKeys(cliente.get(11));
		driver.findElement(salvar).click();
		espera.until(ExpectedConditions.visibilityOfElementLocated(inclusaoSucesso));
		Assert.assertEquals(driver.findElement(inclusaoSucesso).getText(), mensagemInclusaoSucesso);
		
	}
	
	public void voltarParaLista() {
		if(driver.findElement(inclusaoSucesso).isDisplayed())
			driver.findElement(voltarParaLista).click();
		else
			driver.findElement(salvarEVoltarParaLista).click();
	}

}
