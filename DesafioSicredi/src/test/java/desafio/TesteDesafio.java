package desafio;

import java.util.ArrayList;

import org.testng.Reporter;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import paginas.BasePage;
import paginas.ListaClientesPage;
import paginas.NovoClientePage;

public class TesteDesafio {
	
	BasePage navegador;
	NovoClientePage novoCliente;
	ListaClientesPage listaClientes;
	ArrayList<String> cliente;
	String tema;
	String url;
	
	@BeforeTest
	public void preparaTeste() {
		 url = "https://www.grocerycrud.com/v1.x/demo/my_boss_is_in_a_hurry/bootstrap";
		 navegador = new BasePage(url);
		 novoCliente = new NovoClientePage(navegador);
		 listaClientes = new ListaClientesPage(navegador);
		 tema ="Bootstrap V4 Theme";
		 cliente = new ArrayList<String>() ;
				      cliente.add("Teste Sicredi");
				      cliente.add("Teste"); 
				      cliente.add("Erik");
				      cliente.add("51 9999-9999"); 
				      cliente.add("Av Assis Brasil, 3970"); 
				      cliente.add("Torre D"); 
				      cliente.add("Porto Alegre"); 
				      cliente.add("RS"); 
				      cliente.add("91000-000"); 
				      cliente.add("Brasil"); 
				      cliente.add("999"); 
	                  cliente.add("200");

	}
	
	@Test
	public void mudaTemaCriaEDeletaUsuario() {
	
		Reporter.log("Abrindo Navegador");
		navegador.abrirBrowser();
		Reporter.log("Mudando Tema");
		listaClientes.mudarTema(tema);
		Reporter.log("Adicionando Cliente");
		listaClientes.criarCliente();
		novoCliente.adicionarCliente(cliente);
		novoCliente.voltarParaLista();
		Reporter.log("Deletando Cliente");
		listaClientes.deletarCliente(cliente.get(0));
		Reporter.log("Fechando Navegador");
		navegador.fecharNavegador();
	
	}
	

}
